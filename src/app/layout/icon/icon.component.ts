import { Component, HostBinding, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

type IconSize = '' | 'sm' | 'lg' | 'xl';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent implements OnInit, OnChanges {
  @HostBinding('class') internalClassNames: string;
  // tslint:disable-next-line:no-input-rename
  @Input('class') externalClassNames: string;
  @Input() name: string;
  @Input() size: IconSize;

  get src(): string {
    return this.name ? `/assets/svg/sprite.symbol.svg#${this.name}` : '';
  }

  constructor() { }

  private getClassNames(): string {
    const classNames = ['icon'];

    if (this.size) {
      classNames.push(`icon-${this.size}`);
    }
    if (this.externalClassNames) {
      classNames.push(`${this.externalClassNames}`);
    }

    return classNames.join(' ');
  }

  ngOnInit() {
    this.internalClassNames = this.getClassNames();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.size) {
      this.internalClassNames = this.getClassNames();
    }
  }
}
