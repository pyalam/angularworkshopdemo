import { Component, OnInit } from "@angular/core";

interface IMenuItem {
  name: string;
  link: string;
  image: string;
  subMenu?: IMenuItem[];
}

@Component({
  selector: "app-left-menu",
  templateUrl: "./left-menu.component.html",
  styleUrls: ["./left-menu.component.scss"]
})
export class LeftMenuComponent implements OnInit {
  sideNavFlag: boolean;
  menuItems: IMenuItem[] = [
    { name: "DASHBOARD", link: "dashboard", image: "dashboard" },
    {
      name: "Capture Analysis",
      link: "clinicalmanagement",
      image: "request",
      subMenu: [
        { name: "Clinic Analysis", link: "clinicsummary", image: "" },
        {
          name: "Disease State and Drug Analysis",
          link: "drugcapturesummary",
          image: ""
        }
      ]
    },
    {
      name: "Capture Outreach",
      link: "catalog-management",
      image: "catalog",
      subMenu: [
        {
          name: "Capture Outreach Summary",
          link: "outreachsummary",
          image: ""
        },
        {
          name: "Clinic Outreach  Detail",
          link: "clinicoutreachsummary",
          image: ""
        },
        {
          name: "Provider Outreach Detail",
          link: "provideroutreachsummary",
          image: ""
        },
        {
          name: "Disease State and Drug Outreach Detail",
          link: "drugoutreachsummary",
          image: ""
        }
      ]
    },
    { name: "CONTACTS", link: "contacts", image: "users" },
    { name: "ACTIVITIES", link: "activitysummary", image: "rental" },
    { name: "NOTES", link: "notesummary", image: "message" }
  ];
  constructor() {}

  ngOnInit() {}

  collapseClick() {
    this.sideNavFlag = !this.sideNavFlag;
  }

  checkLink(link) {
    //clearing the store data
  }
}
