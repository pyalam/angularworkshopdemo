import { Component, OnInit } from "@angular/core";
import { AboutService } from "./about.service";
@Component({
  selector: "app-about",
  templateUrl: "./about.component.html",
  styleUrls: ["./about.component.scss"]
})
export class AboutComponent implements OnInit {
  inputidvalue: any;
  aboutusername;
  constructor(private aboutservice: AboutService) {}

  ngOnInit() {
    const sessiondata = JSON.parse(sessionStorage.getItem("username"));
    //this.aboutusername = sessiondata["username"];

    this.aboutusername = JSON.parse(
      localStorage.getItem("localstorageusername")
    )["username"];
  }
  deletecustomerinformation() {
    this.aboutservice
      .deletecustomerdata(this.inputidvalue)
      .subscribe((response: any) => {
        console.log(response);
      });
  }
}
