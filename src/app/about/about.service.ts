import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class AboutService {
  constructor(private httpclient: HttpClient) {}

  deletecustomerdata(id) {
    return this.httpclient.delete<any>("http://localhost:3000/customers/" + id);
  }
}
