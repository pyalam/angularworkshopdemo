import { Component, OnInit } from "@angular/core";
import { SharedserviceService } from "../service/sharedservice.service";

@Component({
  selector: "app-child3",
  templateUrl: "./child3.component.html",
  styleUrls: ["./child3.component.scss"]
})
export class Child3Component implements OnInit {
  username3: any;
  constructor(private sharedservice: SharedserviceService) {}

  ngOnInit() {
    //type1 using service
    // this.sharedservice.getName().subscribe(data => (this.username3 = data));
    //type2 using session storage
    //this.username3 = sessionStorage.getItem("username");

    const sessiondata = JSON.parse(sessionStorage.getItem("username"));
    this.username3 = sessiondata["username"];
  }
}
