import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class HomeService {
  constructor(private httpclient: HttpClient) {}

  getcustomersdata() {
    return this.httpclient.get<any>("http://localhost:3000/customers");
  }
  getcustomersdatabyID(id) {
    return this.httpclient.get<any>("http://localhost:3000/customers/" + id);
  }
}
