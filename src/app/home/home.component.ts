import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { HomeService } from "./home.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  customers: any;
  customersingleobject: any;
  inputidvalue: string;
  constructor(private homeservice: HomeService) {}

  ngOnInit() {
    this.homeservice.getcustomersdata().subscribe((response: any) => {
      //console.log(response);
      this.customers = response;
      console.log(this.customers);
    });
  }
  getcustomersbyid() {
    this.homeservice
      .getcustomersdatabyID(this.inputidvalue)
      .subscribe((response: any) => {
        //console.log(response);
        this.customersingleobject = response;
        console.log(this.customers);
      });
  }
}
