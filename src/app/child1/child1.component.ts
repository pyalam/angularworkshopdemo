import { Component, OnInit } from "@angular/core";
import { SharedserviceService } from "../service/sharedservice.service";

@Component({
  selector: "app-child1",
  templateUrl: "./child1.component.html",
  styleUrls: ["./child1.component.scss"]
})
export class Child1Component implements OnInit {
  username1;
  constructor(private sharedservice: SharedserviceService) {}

  ngOnInit() {}

  saveName(event) {
    let inputvalue = document.querySelector("input[name=username]");
    this.username1 = inputvalue["value"];
    //tyep1 using service
    //this.sharedservice.setName(this.username1);

    //type2 using session storage

    var jsonobject = {
      username: this.username1
    };

    //sessionStorage.setItem("username", JSON.stringify(jsonobject));
    localStorage.setItem("localstorageusername", JSON.stringify(jsonobject));
    //this.sharedservice.setName(sessionStorage.getItem("username"));
  }
}
