import { Component, OnInit, Pipe } from "@angular/core";

@Component({
  selector: "app-parent",
  templateUrl: "./parent.component.html",
  styleUrls: ["./parent.component.scss"]
})
export class ParentComponent implements OnInit {
  chars = 1;
  messageToSendP: string;
  receivedChildMessage: string;
  val: string;
  value: string;
  title;
  todaydate;
  jsonval = {
    name: "Rox",
    age: "25",
    address: [
      {
        a1: "delhi",
        a2: "chennai"
      }
    ]
  };

  months = [
    "jan",
    "feb",
    "mar",
    "apr",
    "may",
    "june",
    "july",
    "aug",
    "sep",
    "oct",
    "nov",
    "dec"
  ];

  jsonstring = JSON.stringify(this.jsonval);
  constructor() {}

  ngOnInit() {
    this.title = "Angular Work shop Demo";
    this.todaydate = new Date();
  }
  sendToChild(message: string) {
    this.messageToSendP = message;
  }

  getMessage(message: string) {
    //this.receivedChildMessage = "<b>Response from child :" + message + "</b>";

    this.receivedChildMessage = "Response from child " + message;
  }
  myFunction() {
    //alert("hello this is angular workshop demo");
    this.val = "hey this is the angular workshop demo";
  }
}

@Pipe({ name: "mypipe" })
export class mypipe {
  transform(value) {
    value++;
    return value * 2;
  }
}

@Pipe({ name: "reverse" })
export class reversepipe {
  transform(values: any) {
    return values
      .split("")
      .reverse()
      .join("");
  }
}
