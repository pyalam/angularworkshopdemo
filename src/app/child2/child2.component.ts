import { Component, OnInit } from "@angular/core";
import { SharedserviceService } from "../service/sharedservice.service";

@Component({
  selector: "app-child2",
  templateUrl: "./child2.component.html",
  styleUrls: ["./child2.component.scss"]
})
export class Child2Component implements OnInit {
  username2: any;

  constructor(private sharedservice: SharedserviceService) {}

  ngOnInit() {
    //type 1 using service
    // this.sharedservice
    //   .getName()
    //   .subscribe(data => (this.username2 = JSON.parse(data)));

    const sessiondata = JSON.parse(sessionStorage.getItem("username"));
    this.username2 = sessiondata["username"];
    //type using session storage
    //this.username2 = sessionStorage.getItem("username");
  }
}
