import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ContactService } from "./contact.service";

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.scss"]
})
export class ContactComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  formvalidsubmitted = false;
  contactname;

  constructor(
    private formBuilder: FormBuilder,
    private contactservice: ContactService
  ) {}

  ngOnInit() {
    //const sessiondata = JSON.parse(sessionStorage.getItem("username"));
    //this.contactname = sessiondata["username"];
    const sessiondata = JSON.parse(sessionStorage.getItem("username"));
    //this.aboutusername = sessiondata["username"];

    this.contactname = JSON.parse(localStorage.getItem("localstorageusername"))[
      "username"
    ];
    this.registerForm = this.formBuilder.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }
  onSubmit() {
    //post
    // this.submitted = true;
    // // stop here if form is invalid
    // if (this.registerForm.invalid) {
    //   return;
    // } else {
    //   console.log(
    //     "SUCCESS!! :-)\n\n" + JSON.stringify(this.registerForm.value)
    //   );
    //   this.contactservice
    //     .postcalltoaddcustomerData(this.registerForm.value)
    //     .subscribe((response: any) => {
    //       this.formvalidsubmitted = true;
    //       console.log(response);
    //     });
    // }

    //put call
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      console.log(
        "SUCCESS!! :-)\n\n" + JSON.stringify(this.registerForm.value)
      );
      this.contactservice
        .updatecustomerInformation(2, this.registerForm.value)
        .subscribe((response: any) => {
          this.formvalidsubmitted = true;
          console.log(response);
        });
    }
  }
}
