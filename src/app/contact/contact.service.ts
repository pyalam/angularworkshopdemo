import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ContactService {
  constructor(private httpclient: HttpClient) {}

  postcalltoaddcustomerData(newentry) {
    return this.httpclient.post("http://localhost:3000/customers", newentry);
  }
  updatecustomerInformation(id, newentry) {
    return this.httpclient.put(
      "http://localhost:3000/customers/" + id,
      newentry
    );
  }
  deletecustomerInformation(id) {
    return this.httpclient.delete("http://localhost:3000/customers/" + id);
  }
}
