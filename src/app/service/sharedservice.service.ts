import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class SharedserviceService {
  username = new BehaviorSubject(null);
  username$ = this.username.asObservable();
  constructor() {}

  getName(): Observable<any> {
    return this.username$;
  }
  setName(name) {
    this.username.next(name);
  }
}
