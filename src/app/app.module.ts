import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { HttpClientModule } from "@angular/common/http";

import { LayoutModule } from "./layout/layout.module";
import { HomeComponent } from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { ContactComponent } from "./contact/contact.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatSidenavModule,
  MatIconModule,
  MatButtonModule
} from "@angular/material";
import { NotFoundComponentComponent } from "./not-found-component/not-found-component.component";
import { Child1Component } from "./child1/child1.component";
import { Child2Component } from "./child2/child2.component";
import { Child3Component } from "./child3/child3.component";
import {
  ParentComponent,
  mypipe,
  reversepipe
} from "./parent/parent.component";
import { ChildComponent } from "./child/child.component";
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    NotFoundComponentComponent,
    Child1Component,
    Child2Component,
    Child3Component,
    ParentComponent,
    ChildComponent,
    mypipe,
    reversepipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LayoutModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [MatSidenavModule, MatIconModule, MatButtonModule, HttpClientModule]
})
export class AppModule {}
