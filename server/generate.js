var faker = require("faker");

var database = {
  customers: []
};

for (var i = 1; i <= 300; i++) {
  database.customers.push({
    id: i,
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
    password: faker.internet.password()
  });
}
console.log(JSON.stringify(database));
